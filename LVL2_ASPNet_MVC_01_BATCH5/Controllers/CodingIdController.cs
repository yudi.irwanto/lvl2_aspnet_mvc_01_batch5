﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace LVL2_ASPNet_MVC_01_BATCH5.Controllers
{
    public class CodingIdController : Controller
    {
        // GET: CodingId
        [Route("CodingId")]
        [HttpGet]
        public ActionResult Index()
        {
            string html = "<form method='post'>" +
                "<input type='text' name='name' />" +
                "<input type='submit' value='Greet Me' />" +
                "</form>";
            return Content(html, "text/html");
            //return Content("<h1>Hello World</h1>", "text/html");
        }
        [Route("CodingId")]
        [HttpPost]
        public ActionResult Display(string name = "World")
        {
            return Content(string.Format("<h1>Hello " + name + "</h1>"), "text/html");
        }
        [Route("CodingId/Aloha")]
        public ActionResult Goodbye()
        {
            return Content("Goodbye");
        }
    }
}